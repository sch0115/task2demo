import Vuex from "vuex";
import { shallow, createLocalVue } from "vue-test-utils";
import editEmployeeForm from "@/components/editEmployeeForm.vue";
const localVue = createLocalVue();
const EmployeeListData = [
  {
    firstName: "Eric",
    lastName: "Fruhe",
    date: new Date(
      "Thu Feb 15 1962 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Mon Feb 24 2020 15:37:47 GMT+0100 (Central European Standard Time)"
    ),
    endDate: null,
    position: "product manager",
    status: "Employed",
    id: 1
  },
  {
    firstName: "Steinar",
    lastName: "Thon",
    date: new Date(
      "Tue Feb 13 1973 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Sun Jun 04 2017 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: new Date(
      "Wed Aug 14 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    position: "scrum master",
    status: "Left company",
    id: 2
  },
  {
    firstName: "Anne",
    lastName: "Wang",
    date: new Date(
      "Tue Feb 02 1982 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Tue Apr 09 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: null,
    position: "help desk",
    status: "Employed",
    id: 3
  }
];
localVue.use(Vuex);
describe("editEmployeeForm", () => {
  let store;
  let state;
  beforeEach(() => {
    state = {
      isLoading: false,
      selected: EmployeeListData[0]
    };
    store = new Vuex.Store({
      state
    });
  });
  it("renders editEmployeeForm correctly while in edit mode", () => {
    const wrapper = shallow(editEmployeeForm, {
      store,
      localVue
    });
    expect(wrapper.text()).toMatch("Edit employee record");
  });
  it("renders editEmployeeForm correctly while NOT in edit mode", () => {
    state = {
      isLoading: false,
      selected: null
    };
    store = new Vuex.Store({
      state
    });
    const wrapper = shallow(editEmployeeForm, {
      store,
      localVue
    });
    expect(wrapper.text()).toMatch("Add new employee");
  });

  it("should have its data set to the selected employee", () => {
    const wrapper = shallow(editEmployeeForm, {
      store,
      localVue
    });
    expect(wrapper.vm.employee).toMatchObject(state.selected);
  });
  it("should call updateEmployee after clicking on 'update' button", () => {
    const mutations = {
      editEmployee: jest.fn()
    };
    store = new Vuex.Store({
      state,
      mutations
    });
    const wrapper = shallow(editEmployeeForm, {
      store,
      localVue
    });
    wrapper.$parent = {};
    wrapper.find("#updateButton").trigger("click");
    expect(mutations.editEmployee).toHaveBeenCalled();
  });
  it("should call removeEmployee after clicking on 'remove' button", () => {
    const mutations = {
      removeEmployee: jest.fn()
    };
    store = new Vuex.Store({
      state,
      mutations
    });
    const wrapper = shallow(editEmployeeForm, {
      store,
      localVue
    });
    wrapper.find("#removeButton").trigger("click");
    expect(mutations.removeEmployee).toHaveBeenCalled();
  });
  it("should call addEmployee after clicking on 'add' button", () => {
    const state = {
      selected: null
    };
    const mutations = {
      addEmployee: jest.fn()
    };
    store = new Vuex.Store({
      state,
      mutations
    });
    const wrapper = shallow(editEmployeeForm, {
      store,
      localVue
    });
    wrapper.find("#addButton").trigger("click");
    expect(mutations.addEmployee).toHaveBeenCalled();
  });
});
