import Vuex from "vuex";
import { shallow, createLocalVue } from "vue-test-utils";
import EmployeeList from "@/components/EmployeeList.vue";
const localVue = createLocalVue();
const EmployeeListData = [
  {
    firstName: "Eric",
    lastName: "Fruhe",
    date: new Date(
      "Thu Feb 15 1962 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Mon Feb 24 2020 15:37:47 GMT+0100 (Central European Standard Time)"
    ),
    endDate: null,
    position: "product manager",
    status: "Employed",
    id: 1
  },
  {
    firstName: "Steinar",
    lastName: "Thon",
    date: new Date(
      "Tue Feb 13 1973 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Sun Jun 04 2017 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: new Date(
      "Wed Aug 14 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    position: "scrum master",
    status: "Left company",
    id: 2
  },
  {
    firstName: "Anne",
    lastName: "Wang",
    date: new Date(
      "Tue Feb 02 1982 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Tue Apr 09 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: null,
    position: "help desk",
    status: "Employed",
    id: 3
  }
];
localVue.use(Vuex);
describe("EmployeeList", () => {
  let store;
  let state;
  let mutations;
  beforeEach(() => {
    state = {
      isLoading: false,
      employees: EmployeeListData,
      positions: [],
      selected: null
    };
    mutations = {
      setSelected: jest.fn()
    };
    store = new Vuex.Store({
      state,
      mutations
    });
  });
  it("renders EmployeeList correctly", () => {
    const wrapper = shallow(EmployeeList, {
      store,
      localVue
    });
    expect(wrapper.text()).toMatch("Employee list");
  });
  it("employee list should show all employees when all filters are off", () => {
    const wrapper = shallow(EmployeeList, {
      store,
      localVue
    });

    wrapper.setData({ employedOnly: false });
    expect(wrapper.vm.filteredEmployees).toMatchObject(EmployeeListData);
  });
  it("employee list should show just 2 employees when employed only filter is on", () => {
    // eslint-disable-next-line no-unused-vars
    const wrapper = shallow(EmployeeList, {
      store,
      localVue
    });

    expect(wrapper.vm.filteredEmployees.length).toBe(2);
  });
  it("employee list should show just Anne Wang searching for 'help Wang'", () => {
    const wrapper = shallow(EmployeeList, {
      store,
      localVue
    });
    wrapper.setData({ search: "help Wang" });
    expect(wrapper.vm.filteredEmployees.length).toBe(1);
    expect(
      wrapper.vm.filteredEmployees.some(emp => emp.firstName === "Anne")
    ).toBe(true);
  });
  it("proper function should be called when clicked on the 'new' button", () => {
    const wrapper = shallow(EmployeeList, {
      store,
      localVue
    });
    const functionMock = jest.fn();
    wrapper.vm.onNewClicked = functionMock;
    wrapper.update();

    wrapper.find("button").trigger("click");
    expect(functionMock.mock.calls.length).toBe(1);
  });
});
