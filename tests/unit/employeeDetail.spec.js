import Vuex from "vuex";
import { shallow, createLocalVue } from "vue-test-utils";
import EmployeeDetail from "@/components/EmployeeDetail.vue";
const localVue = createLocalVue();
const EmployeeListData = [
  {
    firstName: "Eric",
    lastName: "Fruhe",
    date: new Date(
      "Thu Feb 15 1962 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Mon Feb 24 2020 15:37:47 GMT+0100 (Central European Standard Time)"
    ),
    endDate: null,
    position: "product manager",
    status: "Employed",
    id: 1
  },
  {
    firstName: "Steinar",
    lastName: "Thon",
    date: new Date(
      "Tue Feb 13 1973 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Sun Jun 04 2017 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: new Date(
      "Wed Aug 14 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    position: "scrum master",
    status: "Left company",
    id: 2
  },
  {
    firstName: "Anne",
    lastName: "Wang",
    date: new Date(
      "Tue Feb 02 1982 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Tue Apr 09 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: null,
    position: "help desk",
    status: "Employed",
    id: 3
  }
];
localVue.use(Vuex);
describe("EmployeeDetail", () => {
  let store;
  let state;
  beforeEach(() => {
    state = {
      isLoading: false,
      selected: EmployeeListData[0]
    };
    store = new Vuex.Store({
      state
    });
  });
  it("renders EmployeeDetail correctly", () => {
    const wrapper = shallow(EmployeeDetail, {
      store,
      localVue
    });
    expect(wrapper.text()).toMatch("Employee detail");
  });
  it("should show name of the selected employee", () => {
    const wrapper = shallow(EmployeeDetail, {
      store,
      localVue
    });

    expect(wrapper.find("h3").text()).toMatch(
      EmployeeListData[0].firstName + " " + EmployeeListData[0].lastName
    );
  });
  it("should call to clear selection after clicking on 'back to overview'", () => {
    const mutations = {
      setSelected: jest.fn()
    };
    store = new Vuex.Store({
      state,
      mutations
    });
    const wrapper = shallow(EmployeeDetail, {
      store,
      localVue
    });
    wrapper.find("button").trigger("click");

    expect(mutations.setSelected).toHaveBeenCalled();
  });
  it("should call proper function after clicking on 'edit'", () => {
    const wrapper = shallow(EmployeeDetail, {
      store,
      localVue
    });
    const functionMock = jest.fn();
    wrapper.vm.onEditClicked = functionMock;
    wrapper.update();

    wrapper.find("#editButton").trigger("click");
    expect(functionMock.mock.calls.length).toBe(1);
  });
});
