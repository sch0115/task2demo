import Vuex from "vuex";
import { shallow, createLocalVue } from "vue-test-utils";
import EmployeeOverview from "@/components/EmployeeOverview.vue";
const localVue = createLocalVue();
const EmployeeListData = [
  {
    firstName: "Eric",
    lastName: "Fruhe",
    date: new Date(
      "Thu Feb 15 1962 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Mon Feb 24 2020 15:37:47 GMT+0100 (Central European Standard Time)"
    ),
    endDate: null,
    position: "product manager",
    status: "Employed",
    id: 1
  },
  {
    firstName: "Steinar",
    lastName: "Thon",
    date: new Date(
      "Tue Feb 13 1973 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Sun Jun 04 2017 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: new Date(
      "Wed Aug 14 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    position: "scrum master",
    status: "Left company",
    id: 2
  },
  {
    firstName: "Anne",
    lastName: "Wang",
    date: new Date(
      "Tue Feb 02 1982 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Tue Apr 09 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: null,
    position: "help desk",
    status: "Employed",
    id: 3
  }
];
localVue.use(Vuex);
describe("EmployeeOverview", () => {
  let store;
  let state;
  beforeEach(() => {
    state = {
      isLoading: false,
      employees: EmployeeListData
    };
    store = new Vuex.Store({
      state
    });
  });
  it("renders EmployeeList correctly", () => {
    const wrapper = shallow(EmployeeOverview, {
      store,
      localVue
    });
    expect(wrapper.text()).toMatch("Employee overview");
  });
  it("employee overview should show count of all emploees", () => {
    // eslint-disable-next-line no-unused-vars
    const wrapper = shallow(EmployeeOverview, {
      store,
      localVue
    });

    expect(wrapper.find("h3").text()).toMatch("Total number of employees: 3");
  });
  it("employee overview should show count of all individual positions", () => {
    const correctPositionsCount = {
      "product manager": 1,
      "scrum master": 1,
      "help desk": 1
    };
    // eslint-disable-next-line no-unused-vars
    const wrapper = shallow(EmployeeOverview, {
      store,
      localVue
    });

    expect(wrapper.vm.positionsCount).toMatchObject(correctPositionsCount);
  });
});
