import Vuex from "vuex";
import { shallow, createLocalVue } from "vue-test-utils";
import EmployeeManagment from "@/views/EmployeeManagment.vue";
const localVue = createLocalVue();
localVue.use(Vuex);
describe("EmployeeManagment", () => {
  let store;
  let actions;
  beforeEach(() => {
    actions = {
      loadEmployees: jest.fn(),
      loadPositions: jest.fn()
    };
    store = new Vuex.Store({
      actions
    });
  });
  it("creation triggers data loading and renders 2 components", () => {
    const wrapper = shallow(EmployeeManagment, {
      store,
      localVue
    });
    expect(actions.loadEmployees.mock.calls).toHaveLength(1);
    expect(actions.loadPositions.mock.calls).toHaveLength(1);
    expect(wrapper.vm.$children[0].$options.name).toBe("EmployeeList");
    expect(wrapper.vm.$children[1].$options.name).toBe("EmployeeOverview");
    expect(wrapper.vm.$children.length).toBe(2);
  });
  it("not null vuex state property selected makes EmployeeDetail instead of EmployeeOverview to render", () => {
    store = new Vuex.Store({
      state: {
        selected: true
      },
      actions
    });
    const wrapper = shallow(EmployeeManagment, {
      store,
      localVue
    });
    expect(wrapper.vm.$children[0].$options.name).toBe("EmployeeList");
    expect(wrapper.vm.$children[1].$options.name).toBe("EmployeeDetail");
    expect(wrapper.vm.$children.length).toBe(2);
  });
});
