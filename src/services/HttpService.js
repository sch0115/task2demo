import Vue from "vue";
import VueResource from "vue-resource";

const MODULE_NAME = "HttpService";

Vue.use(VueResource);

export default {
  name: MODULE_NAME,

  get(url, options = {}) {
    return Vue.http.get(url, options);
  },
  post(url, body, options = {}) {
    return Vue.http.post(url, body, options);
  }
};
