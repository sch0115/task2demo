import { ToastProgrammatic as Toast } from "buefy";

export function notifySuccess(message) {
  Toast.open({ message, type: "is-success", position: "is-top" });
}
export function notifyFailure(message) {
  Toast.open({ message, type: "is-danger", position: "is-top" });
}
