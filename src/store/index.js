import Vue from "vue";
import Vuex from "vuex";
import { notifyFailure, notifySuccess } from "../services/NotificationService";
import HttpService from "../services/HttpService";
const debug = require("debug")("vuexStore");
import { EmployeeListData } from "../assets/EmployeeListData";
import { APP_CONFIG } from "../../app.config";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLoading: false,
    positions: [],
    employees: [],
    selected: null
  },
  mutations: {
    setLoading(state, isLoading) {
      state.isLoading = isLoading;
    },
    setPositions(state, positions) {
      state.positions = positions;
    },
    setSelected(state, selectedEmployee) {
      state.selected = selectedEmployee;
    },
    setEmployees(state, employees) {
      state.employees = employees.map(emp => processEmployee(emp));
    },
    addEmployee(state, employee) {
      employee.id = createNewId(state.employees);
      state.employees.push(processEmployee(employee));
      notifySuccess("Employee has been successfully added");
      //
      // Solution for creating new employee after implementing the server-side
      // This pattern would be applied for edit and remove as well
      // ID would be created on the server
      //
      // HttpService.post(APP_CONFIG.URLs.addEmployee)
      //   .then(response => {
      //     state.employees.push(response.data);
      //     notifySuccess("Employee has been successfully added");
      //   })
      //   .catch(err => {
      //     debug(err);
      //     notifyFailure("Creation of the new employee record failed");
      //   });
    },
    editEmployee(state, employee) {
      const indexOfEmployee = state.employees.findIndex(
        emp => emp.id === employee.id
      );
      if (indexOfEmployee !== -1) {
        const processedEmployee = processEmployee(employee);
        state.employees[indexOfEmployee] = processedEmployee;
        state.employees = [...state.employees];
        state.selected = processedEmployee;
        notifySuccess("Employee successfully updated");
      } else {
        notifyFailure("Employee update failed! Try again later");
      }
    },
    removeEmployee(state, employee) {
      const indexOfEmployee = state.employees.findIndex(
        emp => emp.id === employee.id
      );
      if (indexOfEmployee !== -1) {
        state.employees.splice(indexOfEmployee, 1);
        notifySuccess("Employee successfully removed");
      } else {
        notifyFailure("Employee removal failed! Try again later");
      }
    }
  },
  actions: {
    loadEmployees(context) {
      context.commit("setLoading", true);
      HttpService.get(APP_CONFIG.URLs.loadEmployees)
        .then(response => context.commit("setEmployees", response.data))
        .catch(err => {
          // notifyFailure("Loading of employees from server failed");
          debug(err);
          // Loading from file for purpose of this demo task
          context.commit("setEmployees", EmployeeListData);
          notifySuccess("Employees loaded from backup cache");
        })
        .finally(() => context.commit("setLoading", false));
    },
    loadPositions(context) {
      HttpService.get(APP_CONFIG.URLs.loadPositions)
        .then(response => {
          context.commit("setPositions", response.data.positions);
        })
        .catch(err => {
          notifyFailure("Loading of positions from server failed");
          debug(err);
        });
    }
  }
});

const createNewId = array => {
  if (!array.length) return 1;
  const highestIdMember = array.reduce((a, b) => (a.id > b.id ? a : b));
  return highestIdMember.id + 1;
};

const processEmployee = employee => {
  return {
    ...employee,
    status:
      employee.endDate && employee.endDate < new Date()
        ? "Left company"
        : "Employed"
  };
};
