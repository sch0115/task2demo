export const EmployeeListData = [
  {
    firstName: "Eric",
    lastName: "Fruhe",
    date: new Date(
      "Thu Feb 15 1962 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Mon Feb 24 2020 15:37:47 GMT+0100 (Central European Standard Time)"
    ),
    endDate: null,
    position: "product manager",
    id: 1
  },
  {
    firstName: "Steinar",
    lastName: "Thon",
    date: new Date(
      "Tue Feb 13 1973 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Sun Jun 04 2017 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: new Date(
      "Wed Aug 14 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    position: "scrum master",
    id: 2
  },
  {
    firstName: "Anne",
    lastName: "Wang",
    date: new Date(
      "Tue Feb 02 1982 00:00:00 GMT+0100 (Central European Standard Time)"
    ),
    startDate: new Date(
      "Tue Apr 09 2019 00:00:00 GMT+0200 (Central European Summer Time)"
    ),
    endDate: null,
    position: "help desk",
    id: 3
  }
];
